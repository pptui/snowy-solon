package apis;

import org.junit.runner.RunWith;
import org.noear.solon.test.HttpTestBase;
import org.noear.solon.test.SolonJUnit4ClassRunner;
import org.noear.solon.test.SolonTest;
import vip.xiaonuo.Application;

/**
 * @author noear 2022/10/26 created
 */
@RunWith(SolonJUnit4ClassRunner.class)
@SolonTest(Application.class)
public class ApiDevTest extends HttpTestBase {
}
